"""Extract messages from PSTs and save the messages as EML files"""

from datetime import datetime
from email.message import EmailMessage
from email.policy import default
import glob
import logging
from os import makedirs, path
import re
import sys
from time import perf_counter
import pypff

APP_NAME = "pst_to_eml"
APP_VERSION = "0.6.2"
USAGE = """
usage: pst-to-eml <pst_path> <eml_dir> [<max_msgs>]

    pst_path    Path to the input PST file or directory
    eml_dir     Path to the output EML directory
    max_msgs    Maximum number of messages to process
                Omit to process all messages
                0 to list PST folder message counts in the log
"""

KNOWN_HEADERS = ["Date", "From", "To", "Cc", "Bcc", "Subject"]
KNOWN_HEADERS_LOWER = list(map(lambda x: x.lower(), KNOWN_HEADERS))


def get_args():
    """Validate and return the command line arguments"""
    if not 3 <= len(sys.argv) <= 4:
        print(USAGE)
        raise Exception("invalid number of arguments")

    _, pst_path, eml_dir, *rest = sys.argv
    max_msgs = rest[0] if len(rest) == 1 else float("inf")
    if not path.exists(pst_path):
        raise Exception("pst_path not found: {pst_path}")
    if not path.isdir(eml_dir):
        raise Exception("eml_dir not found: {eml_dir}")
    if max_msgs != float("inf"):
        try:
            max_msgs = int(max_msgs)
        except Exception as exc:
            raise Exception(
                f"max_msgs must be an int: {max_msgs}: {exc}") from exc

    return (pst_path, eml_dir, max_msgs)


def create_logger():
    """Create the global app logger"""
    logging.addLevelName(logging.INFO + 1, "INFO")

    inst = logging.getLogger(__name__)
    inst.setLevel(logging.DEBUG)
    inst.info2 = lambda msg, *args, **kwargs: logger.log(logging.INFO + 1, msg,
                                                         *args, **kwargs)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(logging.Formatter(
        "%(levelname)s %(message)s"))
    console_handler.addFilter(lambda record: record.levelno != logging.INFO)
    inst.addHandler(console_handler)

    log_dir = path.join(path.dirname(sys.argv[0]), "log")
    log_basename = f"{datetime.now():%Y-%m-%d-%H-%M-%S-%f}"
    makedirs(log_dir, exist_ok=True)
    file_handler = logging.FileHandler(
        filename=path.join(log_dir, f"{log_basename[:-3]}.log"),
        encoding="utf-8",)
    file_handler.setFormatter(logging.Formatter(
        "%(asctime)s %(levelname)s %(message)s"))
    file_handler.setLevel(logging.DEBUG)
    inst.addHandler(file_handler)

    return inst


def parse_pst_folder(base_folder, msg_fn, max_msgs=float("inf"), start=1):
    """Parse messages in a PST and pass them to a function for processing"""
    parsed_msgs = 0

    for folder in base_folder.sub_folders:
        logger.info(
            'processing folder "%s" (%d)',
            folder.name,
            folder.number_of_sub_messages)
        if parsed_msgs < max_msgs:
            for message in folder.sub_messages:
                msg_fn(message, start + parsed_msgs)
                parsed_msgs += 1
                if parsed_msgs >= max_msgs:
                    return parsed_msgs
        if folder.number_of_sub_folders:
            parsed_msgs += parse_pst_folder(
                folder,
                msg_fn,
                max_msgs - parsed_msgs,
                start + parsed_msgs)

    return parsed_msgs


def create_eml(pst_msg, pst_path, eml_path):
    """Create EML files from a PST or all PSTs in a directory"""
    logger.info("creating %s", path.basename(eml_path))

    text = pst_msg.plain_text_body
    html = pst_msg.html_body
    attachments = pst_msg.attachments
    header_dict = parse_message_headers(pst_msg.transport_headers)
    eml = EmailMessage(policy=default.clone(max_line_length=998))

    for header, value in header_dict.items():
        header_lower = header.lower()
        if header_lower in KNOWN_HEADERS_LOWER:
            eml[KNOWN_HEADERS[KNOWN_HEADERS_LOWER.index(header_lower)]] = value
        elif header_lower.startswith("x-"):
            eml[header] = value
    process_bbg_headers(eml)
    eml["X-Converter"] = f"{APP_NAME} {APP_VERSION}"
    eml["X-Converter-Input"] = path.basename(pst_path)
    eml["X-Converter-Output"] = path.splitext(path.basename(eml_path))[0]

    if text is not None:
        eml.set_content(text.decode("UTF-8"))
    if html is not None:
        eml.add_alternative(html)
    for attachment in attachments:
        eml.add_attachment(
            attachment.read_buffer(attachment.size),
            maintype="application",
            subtype="octet-stream",
            filename=attachment.name)
    with open(eml_path, 'wb') as eml_file:
        eml_file.write(eml.as_bytes())


def parse_message_headers(headers: str):
    """Parse and unwrap transport headers"""
    lines = headers.split("\r\n")
    hdr_dict = {}
    cur_header = None
    for line in lines:
        if not line.startswith(" "):
            header_parts = line.split(":", 1)
            if len(header_parts) == 2:
                header, value = map(lambda part: part.strip(), header_parts)
                hdr_dict[header] = value
                cur_header = header
            elif not(len(header_parts) == 1 and header_parts[0].strip() == ""):
                logger.warning("header malformed: %s", line)
        elif cur_header is not None:
            hdr_dict[cur_header] = f"{hdr_dict[cur_header]} {line.strip()}"
        else:
            logger.warning("header line skipped: %s", line)

    return hdr_dict


def process_bbg_headers(eml):
    """Create additional headers based on BBG headers"""
    if eml["X-Bloomberg-Message-ID"]:
        bbg_msg_id = eml['X-Bloomberg-Message-ID']
        bbg_room = re.sub("^(chat|adsk)-fs:", r"\1-fs-", bbg_msg_id,
                          1, re.I).split(':')[0][:-2]

        if eml["From"] is None:
            eml["From"] = f"{bbg_room}@unknown"

        if re.match(r"chat-fs:|adsk-fs:|pchat-", bbg_msg_id, flags=re.I):
            if eml["X-Bloomberg-Type"] is None:
                eml["X-Bloomberg-Type"] = "ib"
            eml["X-GlobalRelay-MsgType"] = "Instant Bloomberg"
            eml["X-GR-BBG-RoomName"] = bbg_room
        else:
            if eml["X-Bloomberg-Type"] is None:
                eml["X-Bloomberg-Type"] = "bmail"
            eml["X-GR-BBG-SendingDevice"] = \
                "Terminal" if eml["X-Bloomberg-Device-Type"] is None \
                else "Mobile"


def main():
    """Run the application"""
    try:
        tick = perf_counter()
        exit_code = 0
        total_msgs = 0
        processed_psts = -1

        logger.info2("%s started", sys.argv[0])

        pst_path, eml_dir, max_msgs = get_args()
        logger.info("args: %s, %s, %s", pst_path, eml_dir, max_msgs)
        pst_paths = [pst_path] if not path.isdir(pst_path) \
            else glob.glob(path.join(pst_path, "*.pst"))

        for processed_psts, pst_file in enumerate(pst_paths):
            logger.info2("processing %s", pst_file)
            pst = pypff.file()
            pst.open(pst_file)
            root_folder = pst.get_root_folder()
            eml_path = path.join(eml_dir,
                                 path.splitext(path.basename(pst_file))[0])
            parsed_msgs = 0
            parsed_msgs += parse_pst_folder(
                root_folder,
                lambda message, i, pst_path=pst_file, eml_path=eml_path:
                    create_eml(message, pst_path, f"{eml_path}.{i:06}.eml"),
                max_msgs - total_msgs)
            total_msgs += parsed_msgs
            logger.info2("processed %d message(s) in %s",
                         parsed_msgs, pst_file)

    except Exception as exc:  # pylint: disable=broad-except
        logger.critical(exc, exc_info=exc)
        exit_code = 1

    finally:
        logger.info2("processed %d messages in %s PST(s) in %.2f seconds",
                     total_msgs, processed_psts + 1, perf_counter() - tick)
        logger.info2("%s finished", sys.argv[0])
        sys.exit(exit_code)


if __name__ == "__main__":
    logger = create_logger()
    main()
